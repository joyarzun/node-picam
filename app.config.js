const TOKEN = process.env.TOKEN;
const CHAT_ID = process.env.CHAT_ID;
const photoPath = "./events";

module.exports = {
  TOKEN,
  CHAT_ID,
  photoPath
};
