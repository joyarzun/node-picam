const fs = require("fs");
const Telegraf = require("telegraf");
const { TOKEN, CHAT_ID } = require("../app.config");

const bot = new Telegraf(TOKEN);
process.send({ message: "Loading Telegram" });

bot.command("stopd", ({ reply }) => {
  reply("Stoping detector");
  process.send({ message: "task:detector:stop" });
});
bot.command("startd", ({ reply }) => {
  reply("Loading detector");
  process.send({ message: "task:detector:start" });
});
bot.command("status", ({ reply }) => {
  reply("Loading status");
  process.send({ message: "task:status" });
});
bot.startPolling();

const sendMessage = (text, bot) => bot.telegram.sendMessage(CHAT_ID, text);
const sendPhoto = (imageBuffer, bot) =>
  bot.telegram.sendPhoto(CHAT_ID, { source: imageBuffer });

process.on("message", ({ message, data }) => {
  if (message == "sendPhoto") {
    process.send({ message: "Sending Photo", data });
    sendPhoto(fs.readFileSync(data), bot).then(() =>
      process.send({ message: `Photo ${data} has been sent` })
    );
  }
  if (message == "detector:killed")
    sendMessage(`Detector was killed with signal ${data}`, bot);

  if (message == "task:status") sendMessage(data, bot);
});
