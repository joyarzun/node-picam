const { fork } = require("child_process");
const folderSize = require("./folderSize");

let detector;
let telegram;

const logger = (module, callback) => obj => {
  console.log(
    `message from ${module}: ${obj.message}${obj.data ? " " + obj.data : ""}`
  );
  return callback(obj);
};

const taskSendPhoto = data => telegram.send({ message: "sendPhoto", data });

const taskDetectorStop = () => detector.kill();

const taskDetectorStart = () => {
  detector = fork("src/detector.js");
  detector.on("error", err => console.error(err));
  detector.on(
    "message",
    logger("Detector", ({ message, data }) => {
      if (message == "task:sendPhoto") taskSendPhoto(data);
    })
  );

  detector.on("exit", (_, signal) => {
    telegram.send({ message: "detector:killed", data: signal });
  });

  return detector;
};

const taskStatus = size => {
  const response = `Status:
  Telegram connected: ${telegram.connected}
  Telegram killed: ${telegram.killed}
  Detector connected: ${detector.connected}
  Detector killed: ${detector.killed}
  Events folder size: ${size} Mb`;
  return response;
};

const taskTelegramStart = () => {
  telegram = fork("src/telegram.js");
  telegram.on("error", err => console.error(err));
  telegram.on(
    "message",
    logger("Telegram", ({ message, data }) => {
      if (message == "task:detector:stop") taskDetectorStop();
      if (message == "task:detector:start") taskDetectorStart();
      if (message == "task:status")
        folderSize("./events").then(size =>
          telegram.send({ message: "task:status", data: taskStatus(size) })
        );
    })
  );
  return telegram;
};

module.exports = {
  taskDetectorStart,
  taskTelegramStart
};
