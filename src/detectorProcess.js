const cv = require("opencv4nodejs");
const cam = new cv.VideoCapture(0);

const getFrame = () => cam.read().resizeToMax(800);

const toGrayAndBlur = mat =>
  mat.bgrToGray().gaussianBlur(new cv.Size(21, 21), 0);

const process = (previouslyFrame, lastChange, secToWait) => {
  const originalFrame = getFrame();
  const frame = toGrayAndBlur(originalFrame);
  const deltaFrame = previouslyFrame.absdiff(frame);
  // cv.imshow("a window name", deltaFrame);
  // cv.waitKey();
  const thresh = deltaFrame.threshold(5, 255, cv.THRESH_BINARY);
  const dilatedFrame = thresh.dilate(
    cv.getStructuringElement(cv.MORPH_RECT, new cv.Size(3, 3)),
    new cv.Point(-1, -1),
    2
  );

  // cv.imshow("a window name", dilatedFrame);
  // cv.waitKey();

  const contours = dilatedFrame.findContours(
    cv.RETR_EXTERNAL,
    cv.CHAIN_APPROX_SIMPLE
  );

  const resultframe = originalFrame.copy();

  let maxRect = {
    area: 0
  };
  let newLastChange = lastChange;
  if (new Date().getTime() - lastChange >= secToWait * 1000) {
    contours.forEach(v => {
      if (v.area >= 1000 && maxRect.area < v.area) {
        maxRect = v;
        newLastChange = new Date().getTime();
      }
    });
  }

  if (maxRect.area > 0)
    resultframe.drawRectangle(maxRect.boundingRect(), new cv.Vec(0, 255, 0), 2);

  cv.imshow("a window name", resultframe);
  // cv.waitKey();

  nextFrame = toGrayAndBlur(originalFrame);

  return {
    resultframe,
    newLastChange,
    nextFrame
  };
};

module.exports = process;
module.exports.getFrame = getFrame;
module.exports.toGrayAndBlur = toGrayAndBlur;
