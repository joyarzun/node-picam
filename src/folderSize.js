const { exec } = require("child_process");

const getSize = path =>
  new Promise((resolve, reject) => {
    const cmd = "du -sk " + path;

    exec(cmd, function(err, stdout, stderr) {
      if (err && stderr) {
        return reject(err);
      }
      resolve(parseInt(stdout.trim().match(/^(\d+)/)[1]) / 1024);
    });
  });

module.exports = getSize;
