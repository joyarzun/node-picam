const cv = require("opencv4nodejs");
const detectorProcess = require("./detectorProcess");
const { toGrayAndBlur, getFrame } = require("./detectorProcess");
const { photoPath } = require("../app.config");
const fs = require("fs");

if (!fs.existsSync(photoPath)) fs.mkdirSync(photoPath);
const delay = 1;
let done = false;
let lastChange = new Date().getTime() - 1000 * 60 * 60;
let previouslyFrame = toGrayAndBlur(getFrame());
process.send({ message: "Loading detector" });

while (!done) {
  let { resultframe, newLastChange, nextFrame } = detectorProcess(
    previouslyFrame,
    lastChange,
    3
  );
  if (newLastChange !== lastChange) {
    const photo = `${photoPath}/${newLastChange}.png`;
    cv.imwrite(photo, resultframe);
    process.send({ message: "task:sendPhoto", data: photo });
  }
  lastChange = newLastChange;
  previouslyFrame = nextFrame;

  const key = cv.waitKey(delay);
  done = key !== 255 && key !== -1;
}
