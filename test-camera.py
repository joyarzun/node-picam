import numpy as np
import cv2

cap = cv2.VideoCapture(0) # Capture video from camera



while(cap.isOpened()):
    ret, frame = cap.read()
    if ret == True:
        # frame = cv2.flip(frame,0)


        cv2.imshow('frame',frame)
        if (cv2.waitKey(1) & 0xFF) == ord('q'): # Hit `q` to exit
            break
    else:
        break

# Release everything if job is finished
cap.release()
cv2.destroyAllWindows()